#!/usr/bin/perl -w

my $cfg = $ARGV[0];

print "    Step 1: Gravitational consistency\n";
check_system("./gravitational_consistency", $cfg);

print "    Step 2: Finding parents and cleanup\n";
check_system("./find_parents_and_cleanup", $cfg);

print "    Step 3: Re-sorting outputs\n";
check_system("./resort_outputs", $cfg);

print "    Step 4: Assembling halo trees\n";
check_system("./assemble_halo_trees", $cfg);

sub check_system {
    system(@_) == 0 or
	die "Tree creation failed while executing \"@_\".\n(See errors above).\n";
}
